var app = new Vue({
	el: '#app',
	data: {
		videos: [],
		search: '',
	},

	created() {
		this.fetch();
	},

	methods: {
		fetch() {
			axios.get('./video-search.json')
			.then(function (res) {
				app.videos = res.data.items;
			})
			.catch(function (error) {
				console.log(error.config);
			});
		},

		embed(id) {
			return 'https://www.youtube.com/embed/' + id;
		}
	},

	computed: {
		filteredList() {
			return this.videos.filter((video) => {
				return video.snippet.title.match(this.search);
			});
		}
	}

});

